# Droidcam-RPI

This repository contain Droidcam software optimized to ARM for Raspberry PI. Also the dependencies was solved, libjpeg-turbo is up to compile and install, together the correct libraries path in Raspberry OS

# First Install libjpeg-turbo

The file libjpeg-turbo-1.5.1(RPI) is already compilade to ARM architecture. In the file directoryw execute this

```
../Droidcam-RPI4$ sh ./turbojpeg-install.sh
```

# Second, Install Droidcam

```
../Droidcam-RPI4$ sh ./droidcam-install.sh
```
